﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class Player
    {
        public int Health { get; set; }
        public string Name { get; set; }
        public bool IsDead { get; set; }

        public bool IsGuarding { get; set; }
        public Player()
        {
            Health = 100;
        }

        public void Heal(int amount_to_heal)
        {
            Health = Health + amount_to_heal;



            Console.WriteLine(Name + " has healed " + amount_to_heal + " health. You now have " + Health + " health remaining!");
        }
        public void GetsHit(int hit_value)
        {
            if (IsGuarding)
            {
                Console.WriteLine(Name + " take guard and he is unharmed!" );
                IsGuarding = false;
            }
            else
            {
                Health = Health - hit_value;

                if (Health > 100)
                {
                    Health = 100;
                }

                Console.WriteLine(Name + " were hit for " + hit_value + " damage! now You have " + Health + " Health remaining ");

            }

            if (Health <= 0)
            {
                Die();
            }
        }
        private void Die()
        {
            Console.WriteLine(Name + " Have Died");
            IsDead = true;
        }


    }



}
