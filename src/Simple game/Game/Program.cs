﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    class Program
    {
        static void Main(string[] args)
        {

            Player player = new Player();
            Random random = new Random();

            Console.WriteLine("What is your name?");

            player.Name = Console.ReadLine();

            Console.WriteLine("Thank you for Entering your name " + player.Name + ".\n");

            Enemy firstEnemy = new Enemy("zombies");

            Console.WriteLine(player.Name + " You have encountered " + firstEnemy.Name + "!\n");

            while (!firstEnemy.IsDead && !player.IsDead)
            {

                Console.WriteLine("What would you want to do?\n1. Attack\n2. Defend\n3. Heal ");
                string playersAction = Console.ReadLine();

                switch (playersAction)
                {

                    case "1":
                        Console.WriteLine("You choose to Attack " + firstEnemy.Name + "!");

                        firstEnemy.GetsHit(random.Next(10, 15));
                        break;

                    case "2":
                        Console.WriteLine("You choose to Defend!");
                        player.IsGuarding = true;

                        break;
                    case "3":
                        Console.WriteLine("You choose to Heal! ");

                        player.Heal(random.Next(10, 15));

                        break;
                    default:
                        Console.WriteLine("You choose something else");
                        break;

                }

                player.GetsHit(random.Next(1, 5));

            }
            Console.ReadKey();
        }

    }
}
