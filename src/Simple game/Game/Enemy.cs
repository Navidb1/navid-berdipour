﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class Enemy
    {
        public int Health { get; set; }
        public string Name { get; set; }

        public bool IsDead { get; set; }

        public Enemy(string name)
        {
            Health = 100;
            Name = name;
        }


        public void GetsHit(int hit_value)
        {
            Health = Health - hit_value;

            Console.WriteLine(Name + " were hit for " + hit_value + " damage! He has now "+ Health +" remaining ");

            if (Health <= 0)
            {
                Die();
            }


        }
        private void Die()
        {
            Console.WriteLine(Name + " Have Died");
            IsDead = true;
        }


    }

}
